#!/usr/bin/env python

#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements. See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership. The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License. You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.
#

import sys
import glob

import logging
logging.basicConfig(level=logging.DEBUG)

sys.path.append('gen-py')

from chord import FileStore
from chord.ttypes import SystemException, RFileMetadata, RFile, NodeID

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol


def main(serverip,portno):
    # Make socket
    transport = TSocket.TSocket(serverip, portno)

    # Buffering is critical. Raw sockets are very slow
    transport = TTransport.TBufferedTransport(transport)

    # Wrap in a protocol
    protocol = TBinaryProtocol.TBinaryProtocol(transport)

    # Create a client to use the protocol encoder
    client = FileStore.Client(protocol)

    # Connect!
    transport.open()

    newFile = RFile()
    newFile.content = "Some random content here"
    newFile.meta = RFileMetadata()
    newFile.meta.filename = "Randomfilename.txt"
    newFile.meta.version = 0
    newFile.meta.owner = "kartik"
    newFile.meta.contentHash = "somehash"
    client.writeFile(newFile)


    try:
        bFile = client.readFile("Randomfilename.txt", "kartik")
        print("File read from the server is " + str(bFile))
    except SystemException:
        print("File not found")

    try:
        aFile = client.readFile("Randomfilename.txt", "fakeuser")
        print("File read from the server is " + str(aFile))
    except SystemException:
        print("File not found")

    try:
        aNode = client.getNodeSucc()
        print("This is the successor node: " + str(aNode))
    except SystemException:
        print("Successor not found.")

    # Close!
    transport.close()


if __name__ == '__main__':
    try:
	if len(sys.argv) != 3:
                print "Please pass a server's IP and portNumber you wish to connect to"
                sys.exit(0)
        main(sys.argv[1],sys.argv[2])
    except Thrift.TException as tx:
        print('%s' % tx.message)
