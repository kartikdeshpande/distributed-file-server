# Distributed-File-Server
This file server is a chord research paper implementation. 

## Requirenments

- Python 2.7
- Apache thrift


## File Server 
The FileServer.py is the main file. 

Functions which the  FileServer supports -:

- **writeFile** given a name, owner, and contents, the corresponding file should be written to the server. Meta-
information, such as the owner, version, and content hash (use the SHA-256 hash) will be stored
at the server side.
If the filename does not exist on the server, a new file will be created with its version attribute set to 0.
Otherwise, the file contents will be overwritten and the version number will be incremented.
If the server does not own the file’s id, a SystemException will be thrown.

- **readFile** if a file with a given name and owner exists on the server, both the contents and meta-information will
be returned. Otherwise, a SystemException will be thrown and appropriate information indicating the
cause of the exception will be included in the SystemException’s message field.
If the server does not own the file’s id, a SystemException will be thrown.

- **setFingertable** will set the current node’s fingertable to the fingertable provided in the argument of the function. The init script will call this function. 

- **findSucc** given an identifier in the DHT’s key space, will returns the DHT node that owns the id. This function is
be implemented in two parts: 
  - The function will call findPred to discover the DHT node that precedes the given id
  - The function will call getNodeSucc to find the successor of this predecessor node

- **findPred** given an identifier in the DHT’s key space, this function will return the DHT node that immediately
precedes the id. This preceeding node is the first node in the counter-clockwise direction in the Chord key
space. A SystemException will be thrown if no fingertable exists for the current node.

- **getNodeSucc** returns the closest DHT node that follows the current node in the Chord key space. A SystemEx-
ception will be thrown if no fingertable exists for the current node.

Running a server instance 
Give the port number as argument to which the server should bind to.

```
python FileServer.py PORTNUMBER

```


## Initializer program
init script is the script that initialises finger table in all nodes.

The initializer program takes a filename as its only command line argument. To run the initializer:

```
  - chmod +x init
  - ./init nodex.txt
```
  
The file (node.txt) will contain a list of IP addresses and ports, in the format “<ip-address>:<port>”, of
all of the running DHT nodes.
For example, if four DHT nodes are running on 128.226.180.163 port 9090, 9091, 9092,
and 9093, then nodes.txt will contain:

```
128.226.180.163:9090
128.226.180.163:9091
128.226.180.163:9092
128.226.180.163:9093
```

## Client

Basic operations. 

- **ReadFile and writeFile**  Calls read file and write file functions on server by providing filename m owner and meta information. 

Running a client -

Give the serverip and serverport you wish to connect to

```
python Client.py ServerIP ServerPort

```

## Generating service stubs

chord.thrift file in the root directory is used to generate service stubs for RPC framework

```
thrift -gen py chord.thrift

```

## References
- Ion Stoica, Robert Morris, David Karger, M. Frans Kaashoek, and Hari Balakrishnan. Chord: A Scalable Peer-
to-peer Lookup Service for Internet Applications. In Proceedings of the ACM SIGCOMM ’01 Conference, San
Diego, California, August 2001.
- Apache thrift https://thrift.apache.org/tutorial/


### Contact

[Kartik Deshpande](https://www.linkedin.com/in/kartik-deshpande/)
