#!/usr/bin/env python

import glob
import sys
import os
import hashlib
import logging
import socket    # Only used to get the IP address of local machine
import shutil

logging.basicConfig(level=logging.DEBUG)

sys.path.append('gen-py')

from chord import FileStore
from chord.ttypes import SystemException, RFileMetadata, RFile, NodeID

from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer

UPLOADDIRECTORY = "./uploads/"

def sha256(hashstring):
	return hashlib.sha256(hashstring).hexdigest()

class FileStoreHandler:
	
	MYIP = ""
	MYPORT = ""
    	finger_table = []
    	file_server_storage_database = {}
    	file_contents = {}
    	ownid = ""

    	def __init__(self,serverip,port):
		
		# initialize all variables
        	self.log = {}
		self.MYIP = serverip
		self.MYPORT = port

	#######################################
	## Function to check if finger table initialized or not	
	def check_finger_table_initialization(self):
		if len(self.finger_table) < 1 :
			raise SystemException("Finger table is not initialized. Run ./init script to initialize finger table ")

	##########################################
	## Function to write file	
	def writeFile(self, rFile):
		
		#to check if finger table initialized or not, if not raise exception
                self.check_finger_table_initialization()

		# find the hash of file and its owner	
		key_id = sha256(rFile.meta.owner + ":" + rFile.meta.filename) 

		# find the successor node for the key
		MYSuccessorNode = self.findSucc(key_id)

		# if key does not belong to me, perform an RPC to next server walking down the ring
		if str(self.MYIP) != str(MYSuccessorNode.ip) or str(self.MYPORT) != str(MYSuccessorNode.port) :
			try:
		                transport = TSocket.TSocket(host=MYSuccessorNode.ip, port=int(MYSuccessorNode.port))
        		        transport = TTransport.TBufferedTransport(transport)
        	        	protocol = TBinaryProtocol.TBinaryProtocol(transport)
        	        	client = FileStore.Client(protocol);
        	        	transport.open()
        	       	 	client.writeFile(rFile)
        	        	transport.close()
			except:
				raise SystemException("Error connecting to " + str(MYSuccessorNode.ip) + " " + str(MYSuccessorNode.port))
		else:
			
			# key belongs to me
			
			if key_id in self.file_server_storage_database and len(self.file_server_storage_database) > 0:
				# check if valid owner writing file
                        	if rFile.meta.owner == self.file_server_storage_database[key_id].owner : 
					# overwrite file
					with open( UPLOADDIRECTORY + rFile.meta.filename , 'w+') as f:
						f.write(rFile.content)

					self.file_server_storage_database[key_id].version += 1
	                	        self.file_server_storage_database[key_id].contentHash = hashlib.sha256(rFile.content)
        	        	        self.file_contents[key_id] = rFile.content
				else:
					raise SystemException("ABORT ! The file " + rFile.meta.filename + " is not owned by you.")			
			else:
				with open( UPLOADDIRECTORY + rFile.meta.filename ,"a+") as f:
					f.write(rFile.content)
				self.file_contents[key_id] = rFile.content
                        	self.file_server_storage_database[key_id] = RFileMetadata()
                        	self.file_server_storage_database[key_id].filename = rFile.meta.filename
                        	self.file_server_storage_database[key_id].version = 0
                        	self.file_server_storage_database[key_id].owner = rFile.meta.owner
                        	self.file_server_storage_database[key_id].contentHash = hashlib.sha256(rFile.content).hexdigest()
			return "WriteFile -> File " + rFile.meta.filename + " written to server " + str(self.MYIP) + ":" + str(self.MYPORT)


	##########################################
	## Function readfile : Check if file with right owner exist on server, if yes return file else raise an exception
    	def readFile(self, filename, owner):

		#to check if finger table initialized or not, if not raise exception
                self.check_finger_table_initialization()
		
		# find hash of owener and filename
		key_id = sha256(owner + ":" +  filename)

		# find the successor of this key
                MYSuccessorNode = self.findSucc(key_id)

		# if key does not belong to me, perform an RPC to next server walking down the ring
                if str(self.MYIP) != str(MYSuccessorNode.ip) or str(self.MYPORT) != str(MYSuccessorNode.port) :
			try:
	                        transport = TSocket.TSocket(host=MYSuccessorNode.ip, port=int(MYSuccessorNode.port))
        	                transport = TTransport.TBufferedTransport(transport)
        	                protocol = TBinaryProtocol.TBinaryProtocol(transport)
        	                client = FileStore.Client(protocol);
        	                transport.open()
        	                client.readFile(filename, owner)
        	                transport.close()
			except:
                                raise SystemException("Error connecting to " + str(MYSuccessorNode.ip) + " " + str(MYSuccessorNode.port))
                else:
			# key belongs to me

			if key_id not in self.file_server_storage_database:
                        	raise SystemException("The requested file " + filename + " was not found ")
                	else:	
				## For attributes of RFile refer gen-py/chord/ttypes.py file
            			returnFile = RFile()
            			returnFile.meta = RFileMetadata()
            			returnFile.content = self.file_contents[key_id]
            			returnFile.meta.filename = self.file_server_storage_database[key_id].filename
            			returnFile.meta.version = self.file_server_storage_database[key_id].version
	            		returnFile.meta.owner = self.file_server_storage_database[key_id].owner
        	    		returnFile.meta.contentHash = str(self.file_server_storage_database[key_id].contentHash)

                	        # check if valid owner accessing file
                        	if returnFile.meta.owner != owner :
                        	        raise SystemException("ABORT ! The file " + filename + " is not owned by the rightful owner")
                        	else:
                        	        return returnFile;

	############################################
	## Function to store finger table into a list
    	def setFingertable(self, node_list):
		print "Finger table initialized...."
        	self.finger_table = node_list

	############################################
	## Function to return succ node for given key_id
    	def findSucc(self, key_id):
		# check if current node is successor of of specified key_id

                currentNodeKey = sha256(self.MYIP + ":" + self.MYPORT)
		predecessorNode = self.findPred(key_id)

		if str(predecessorNode.ip) == str(self.MYIP) and str(predecessorNode.port) == str(self.MYPORT):
			successorNode = self.getNodeSucc()
		else:
			try:
		                transport = TSocket.TSocket(host=predecessorNode.ip, port=int(predecessorNode.port))
        	        	transport = TTransport.TBufferedTransport(transport)
        	        	protocol = TBinaryProtocol.TBinaryProtocol(transport)
        	        	client = FileStore.Client(protocol);
        	        	transport.open()
        	        	successorNode = client.getNodeSucc()
        	        	transport.close()
			except:
                                raise SystemException("Error connecting to " + str(predecessorNode.ip) + " " + str(predecessorNode.port))


		return successorNode


	########################################
	## TO find predecessor node of given key_id	
    	def findPred(self, key_id):
		#to check if finger table initialized or not, if not raise exception
                self.check_finger_table_initialization()

                currentNodeKey = sha256(self.MYIP + ":" + self.MYPORT)

                current_Node = NodeID(currentNodeKey,self.MYIP,self.MYPORT)

		# check if key_id lies between currentNode and first entry in finger table
		Node_1 = self.finger_table[0]
		if currentNodeKey > Node_1.id: 
			if key_id >= currentNodeKey or key_id <= Node_1.id :
	                   	Predecessor =  current_Node
			else:
				Next_Node = self.Get_Next_Node(key_id)
				if Next_Node.id == currentNodeKey:
					Predecessor = Next_Node
				else:
					try:
                				transport = TSocket.TSocket(host=Next_Node.ip, port=int(Next_Node.port))
                				transport = TTransport.TBufferedTransport(transport)
                				protocol = TBinaryProtocol.TBinaryProtocol(transport)
                				client = FileStore.Client(protocol);
                				transport.open()
                				Predecessor = client.findPred(key_id)
                				transport.close()
					except:
		                                raise SystemException("Error connecting to " + str(Next_Node.ip) + " " + str(Next_Node.port))

		else:
			if key_id > currentNodeKey:
				if key_id <= Node_1.id:
					Predecessor = current_Node
				else:
					Next_Node = self.Get_Next_Node(key_id)
               	                 	if Next_Node.id == currentNodeKey:
                                        	Predecessor = Next_Node
                                	else:
						try:
        	                                	transport = TSocket.TSocket(host=Next_Node.ip, port=int(Next_Node.port))
                	                        	transport = TTransport.TBufferedTransport(transport)
                        	                	protocol = TBinaryProtocol.TBinaryProtocol(transport)
                                	        	client = FileStore.Client(protocol);
                                        		transport.open()
                                        		Predecessor = client.findPred(key_id)
                                        		transport.close()
						except:
	                                                raise SystemException("Error connecting to " + str(Next_Node.ip) + " " + str(Next_Node.port))
			else:
				Next_Node = self.Get_Next_Node(key_id)
                                if Next_Node.id == currentNodeKey:
                                        Predecessor = Next_Node
                                else:
					try:
                                        	transport = TSocket.TSocket(host=Next_Node.ip, port=int(Next_Node.ip))
                                        	transport = TTransport.TBufferedTransport(transport)
                                        	protocol = TBinaryProtocol.TBinaryProtocol(transport)
                                        	client = FileStore.Client(protocol);
                                        	transport.open()
                                        	Predecessor = client.findPred(key_id)
                                        	transport.close()
					except:
                                                raise SystemException("Error connecting to " + str(Next_Node.ip) + " " + str(Next_Node.port))
		return Predecessor

	#######################################
        ## to return target node
        def Get_Next_Node(self,key_id):
                #to check if finger table initialized or not, if not raise exception
                self.check_finger_table_initialization()

                Counter1 = 0
                Counter2 = 1
                loop_flag=1

                while loop_flag == 1:
                        Node_1 = self.finger_table[Counter1]
                        Node_2 = self.finger_table[Counter2]

                        if Node_1.id > Node_2.id :
                                if key_id >= Node_1.id or key_id <= Node_2.id:
                                        Next_Node = Node_1
                                        break
                                else:
                                        Counter1 = Counter1 + 1
                                        Counter2 = Counter2 + 1
                                        if Counter2 >= len(self.finger_table):
                                                loop_flag = 0
	                                      	Next_Node = self.finger_table[len(self.finger_table) - 1]
						break
                        else:
                                if key_id > Node_1.id:
                                        if key_id <= Node_2.id:
                                                Next_Node = Node_1
                                                break
                                        else:
                                                Counter1 = Counter1 + 1
                                                Counter2 = Counter2 + 1
                                                if Counter2 >= len(self.finger_table):
                                                        loop_flag = 0
                                                        Next_Node = self.finger_table[len(self.finger_table) - 1]
                                                        break
                                else:
                                        Counter1 = Counter1 + 1
                                        Counter2 = Counter2 + 1
                                        if Counter2 >= len(self.finger_table):
                                                loop_flag = 0
                                                Next_Node = self.finger_table[len(self.finger_table) - 1]
                                                break
                return Next_Node
	


	############################################
	## Get successor node 
    	def getNodeSucc(self):
		#to check if finger table initialized or not, if not raise exception
                self.check_finger_table_initialization()

		return self.finger_table[0]


if __name__ == '__main__':

    	if len(sys.argv) != 2:
        	print "Please pass a port number where server is to be started."
        	sys.exit(0)

	# Delete everythin from UPLOADDIRECTORY at start
	if os.path.exists(UPLOADDIRECTORY):
		shutil.rmtree(UPLOADDIRECTORY)

	# Create new directory UPLOADDIRECTORY at start
        os.makedirs(UPLOADDIRECTORY)

	try:
	    	server_ip = socket.gethostbyname(socket.gethostname())
    		handler = FileStoreHandler(sys.argv[1],server_ip)
    		processor = FileStore.Processor(handler)
    		transport = TSocket.TServerSocket(port=sys.argv[1])
    		tfactory = TTransport.TBufferedTransportFactory()
    		pfactory = TBinaryProtocol.TBinaryProtocolFactory()
   	 	server = TServer.TSimpleServer(processor, transport, tfactory, pfactory)
    		print('Starting the File Server on ' + str(server_ip) + ':' + sys.argv[1] + '...')
    		server.serve()
	except SystemException:
                print "Probably a port already in use error "
